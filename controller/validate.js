var showMesage = function (id, message) {
  document.getElementById(id).innerHTML = `<strong>${message}</strong>`;
};
function showErr(id) {
  document.getElementById(id).style.display = "block";
}
var kiemTraRong = function (id, value) {
  console.log("value: ", value);
  if (value.length == 0 || value == 0) {
    showErr(id);
    showMesage(id, "Trường này không được bỏ trống");
    return false;
  } else {
    showMesage(id, "");
    return true;
  }
};
var kiemTraTrung = function (id, value) {
  var viTri = dsnv.findIndex(function (item) {
    return value == item.id;
  });
  if (viTri >= 0) {
    showErr(id);
    showMesage(id, "Tài khoản đã bị trùng");
    return false;
  } else {
    showMesage(id, "");
    return true;
  }
};

// test id
function kiemTraTaiKhoan(id, value) {
  var test1 = false;
  if (value.length >= 4 && value.length <= 6) {
    showMesage(id, "");
    test1 = true;
  }
  var test2 = false;
  if (value * 1 == value) {
    test2 = true;
  }

  if (test1 == false) {
    showErr(id);
    showMesage(id, "Tài Khoản có độ dài không hợp lệ -độ dài 4 - 6 ký số");
    return false;
  }
  if (test2 == false) {
    showErr(id);
    showMesage(id, "Tài khoản chưa hợp lệ phải là chuỗi ký số");
    return false;
  }
  return true;
}

// test name
function kiemTraTen(id, value) {
  var resuft = /^([^0-9]*)$/.test(value);
  if (resuft) {
    showMesage(id, "");
    return true;
  } else {
    showErr(id);
    showMesage(id, "Tên không hợp lệ");
    return false;
  }
}
// test email
function kiemTraMail(id, value) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var resuft = re.test(value);
  if (resuft == false) {
    showErr(id);
    showMesage(id, "Email không hợp lệ");
    return false;
  } else {
    showMesage(id, "");
    return true;
  }
}

// test pass
function kiemTraMatKhau(id, value) {
  var re = /^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z]).{6,10}$/;
  var resuft = re.test(value);
  if (resuft == false) {
    showErr(id);
    showMesage(
      id,
      "Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
    return false;
  } else {
    showMesage(id, "");
    return true;
  }
}
// test date
function kiemTraNgay(id, value) {
  var re = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
  var resuft = re.test(value);
  if (resuft == false) {
    showErr(id);
    showMesage(id, "Ngày được định dạng mm/dd/yyyy");
    return false;
  } else {
    showMesage(id, "");
    return true;
  }
}
// test salary

var kiemTraLuong = function (id, value) {
  if (value * 1 >= 1000000 && value * 1 <= 20000000) {
    showMesage(id, "");
    return true;
  } else {
    showErr(id);
    showMesage(id, "Lương cơ bản 1 000 000 - 20 000 000");
    return false;
  }
};
// test regency
function kiemTraChucVu(id, value) {
  if (value == "Sếp" || value == "Trưởng phòng" || value == "Nhân viên") {
    showMesage(id, "");
    return true;
  } else {
    showErr(id);
    showMesage(
      id,
      "Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)"
    );
    return false;
  }
}
// test hour
function kiemTraGioLam(id, value) {
  if (value * 1 >= 80 && value * 1 <= 200) {
    showMesage(id, "");
    return true;
  } else {
    showErr(id);
    showMesage(id, "Số giờ làm trong tháng 80 - 200 giờ");
    return false;
  }
}
