function layThongTinTuForm() {
  // lấy thông tin từ thẻ input
  var id = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var pass = document.getElementById("password").value;
  var date = document.getElementById("datepicker").value;
  var salary = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu");
  var regency = chucVu.options[chucVu.selectedIndex].text;
  var hours = document.getElementById("gioLam").value * 1;
  var staff = new NhanVien(id, name, email, pass, date, salary, regency, hours);
  return staff;
}
function renderNV(ds) {
  var contentHTML = ``;
  for (var i = 0; i < ds.length; i++) {
    var nv = ds[i];
    var contentRow = `
        <tr>
        <td>${nv.id}</td>
        <td>${nv.name}</td>
          <td>${nv.email}</td>
          <td>${nv.date}</td>
          <td>${nv.regency}</td>
            <td>${nv.totalSalary()}</td>
            <td>${nv.typeStaff()}</td>
            <td>
            <button class = "btn btn-success" onclick = "xoaNV('${
              nv.id
            }')">Xóa</button>
            <button data-toggle="modal" data-target="#myModal" class = "btn btn-primary" onclick = "suaNV('${
              nv.id
            }')">Sủa</button>
            </td>
        </tr>
        `;
    contentHTML += contentRow;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function validateForm(nv) {
  // Kiem tra validate
  // id
  var isValue =
    kiemTraRong("tbTKNV", nv.id.toString()) &&
    kiemTraTaiKhoan("tbTKNV", nv.id.toString()) ;
  // name
  isValue =
    isValue &
    (kiemTraRong("tbTen", nv.name.toString()) &&
      kiemTraTen("tbTen", nv.name.toString()));
  // mail
  isValue =
    isValue &
    (kiemTraRong("tbEmail", nv.email.toString()) &&
      kiemTraMail("tbEmail", nv.email.toString()));
  // pass
  isValue =
    isValue &
    (kiemTraRong("tbMatKhau", nv.pass.toString()) &&
      kiemTraMatKhau("tbMatKhau", nv.pass.toString()));
  // date
  isValue =
    isValue &
    (kiemTraRong("tbNgay", nv.date.toString()) &&
      kiemTraNgay("tbNgay", nv.date.toString()));
  // salary
  isValue =
    isValue &
    (kiemTraRong("tbLuongCB", nv.salary) &&
      kiemTraLuong("tbLuongCB", nv.salary));
  // regency
  isValue =
    isValue &
    (kiemTraRong("tbChucVu", nv.regency.toString()) &&
      kiemTraChucVu("tbChucVu", nv.regency));
  //  hour
  isValue =
    isValue &
    (kiemTraRong("tbGiolam", nv.hours) && kiemTraGioLam("tbGiolam", nv.hours));
  return isValue;
}

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.id;
  document.getElementById("name").value = nv.name;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.pass;
  document.getElementById("luongCB").value = nv.salary;
  document.getElementById("chucvu").value = nv.regency;
  document.getElementById("gioLam").value = nv.hours;
}

