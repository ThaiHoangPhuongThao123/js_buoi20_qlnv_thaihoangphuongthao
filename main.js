// Khởi tạo mảng lấy danh sách nhân viên
var dsnv = [];
var dataJSON = localStorage.getItem("DSSV_LOCAL");

if (dataJSON != null) {
  var arrData = JSON.parse(dataJSON);
  for (var i = 0; i < arrData.length; i++) {
    var item = arrData[i];
    var nv = new NhanVien(
      item.id,
      item.name,
      item.email,
      item.pass,
      item.date,
      item.salary,
      item.regency,
      item.hours
    );
    dsnv.push(nv);
    // render dsnv
  }
  renderNV(dsnv);
}
// Hàm thêm nhân viên vào danh sách
function ThemNV() {
  // Lấy thông tin từ form
  var nv = layThongTinTuForm();
  //    Thêm nhân viên vào mảng

  var isVali = validateForm(nv) && kiemTraTrung("tbTKNV", nv.id.toString());
  if (isVali) {
    dsnv.push(nv);
    // render dsnv
    renderNV(dsnv);
    // convert data => sàn kiểu Json
    var dataJSON = JSON.stringify(dsnv);
    // lưu data xuống localStorage
    localStorage.setItem("DSSV_LOCAL", dataJSON);
    resetData()
  }
}

function xoaNV(id) {
  var viTri = dsnv.findIndex(function (item) {
    return id == item.id;
  });
  if (viTri >= 0) {
    dsnv.splice(viTri, 1);
    renderNV(dsnv);
  }
}
function suaNV(id) {
  document.getElementById("tknv").disabled = true;
  var viTri = dsnv.findIndex(function (item) {
    return id == item.id;
  });
  if (viTri >= 0) {
    var nv = showThongTinLenForm(dsnv[viTri]);
  }
  return -1;
}

function capNhapThongTin() {
  document.getElementById("tknv").disabled = false;
  var sv = layThongTinTuForm();
  var viTri = dsnv.findIndex(function (item) {
    return item.id == sv.id;
  });
  var validate = validateForm(sv);

  if (viTri >= 0 && validate == true) {
    dsnv[viTri] = sv;
    renderNV(dsnv);
  }
  resetData();
}

function resetData() {
  document.getElementById("tknv").value = "";
  document.getElementById("name").value = "";
  document.getElementById("email").value = "";
  document.getElementById("password").value = "";
  document.getElementById("datepicker").value = "";
  document.getElementById("luongCB").value = "";
  document.getElementById("chucvu").options.text = "";
  document.getElementById("gioLam").value = "";
}
function close111() {
  document.getElementById("tknv").disabled = false;
  resetData();
}

function findText() {
  var dstk = [];
  var key = document.getElementById("searchName").value;
  key = key.toLowerCase();
  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    var text = nv.typeStaff();
    var positon = text.search(key);
    if (positon >= 0) {
      dstk.push(nv);
    }
  }
  renderNV(dstk);
}
