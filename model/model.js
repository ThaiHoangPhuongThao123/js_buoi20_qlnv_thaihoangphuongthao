function NhanVien(
  _id,
  _name,
  _email,
  _pass,
  _date,
  _salary,
  _regency,
  _hours,
  _totalSalary,
  _typeStaff
) {
  this.id = _id;
  this.name = _name;
  this.email = _email;
  this.pass = _pass;
  this.date = _date;
  this.salary = _salary;
  this.hours = _hours;
  this.regency = _regency;
  this.totalSalary = function () {
    if ((this.regency = "Sếp")) {
      return this.salary * 3;
    } else if ((this.regency = "Trưởng phòng")) {
      return this.salary * 2;
    } else if ((this.regency = "Nhân viên")) {
      return this.salary;
    }
  };

  this.typeStaff = function () {
    if (this.hours >= 192) {
      return "Nhân viên xuất sắc"
    } else if(this.hours >= 176) {
      return "Nhân viên giỏi"
    }else if(this.hours >= 160) {
      return "Nhân viên khá"
    } else {
      return "nhân viên trung bình"
    }
  }; 
}
